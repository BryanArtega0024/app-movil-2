import 'package:cartoons_flutter/model/character.dart';

final characters = [
  Character(
    name: 'Hyundai',
    age: 28,
    image: 'images/1.png',
    jobTitle: 'Baja Gama',
    stars: 3.3,
  ),
  Character(
    name: 'Tecno',
    age: 21,
    image: 'images/2.png',
    jobTitle: 'Baja Gama',
    stars: 3.2,
  ),
  Character(
    name: 'Poco',
    age: 33,
    image: 'images/3.png',
    jobTitle: 'Alta Gama',
    stars: 4.9,
  ),
  Character(
    name: 'Iphone',
    age: 29,
    image: 'images/4.png',
    jobTitle: 'Alta Gama',
    stars: 5.1,
  ),
  Character(
    name: 'Iphone',
    age: 24,
    image: 'images/5.png',
    jobTitle: 'Alta Gama',
    stars: 5.5,
  ),
  Character(
    name: 'Redmi',
    age: 19,
    image: 'images/6.png',
    jobTitle: 'Media Gama',
    stars: 4.9,
  ),
  Character(
    name: 'Realme',
    age: 35,
    image: 'images/7.png',
    jobTitle: 'Baja Gama',
    stars: 3.8,
  ),
  Character(
    name: 'Redmi',
    age: 31,
    image: 'images/8.png',
    jobTitle: 'Media Gama',
    stars: 4.6,
  ),
  Character(
    name: 'Vivo',
    age: 22,
    image: 'images/9.png',
    jobTitle: 'Media Gama',
    stars: 4.0,
  ),
  Character(
    name: 'Huawei',
    age: 27,
    image: 'images/10.png',
    jobTitle: 'Baja Gama',
    stars: 3.9,
  ),
];
