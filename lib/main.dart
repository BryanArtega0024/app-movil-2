import 'package:cartoons_flutter/model/character.dart';
import 'package:flutter/material.dart';
import 'package:cartoons_flutter/model/characters_data.dart';
import 'package:cartoons_flutter/widgets/character_widget.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key? key}) : super(key: key);

  final characters = [
    Character(
      name: 'Hyundai',
      age: 28,
      image: 'images/1.png',
      jobTitle: 'Baja Gama',
      stars: 3.3,
    ),
    Character(
      name: 'Tecno',
      age: 21,
      image: 'images/2.png',
      jobTitle: 'Baja Gama',
      stars: 3.2,
    ),
    Character(
      name: 'Poco',
      age: 33,
      image: 'images/3.png',
      jobTitle: 'Alta Gama',
      stars: 4.9,
    ),
    Character(
      name: 'Iphone',
      age: 29,
      image: 'images/4.png',
      jobTitle: 'Alta Gama',
      stars: 5.1,
    ),
    Character(
      name: 'Iphone',
      age: 24,
      image: 'images/5.png',
      jobTitle: 'Alta Gama',
      stars: 5.5,
    ),
    Character(
      name: 'Redmi',
      age: 19,
      image: 'images/6.png',
      jobTitle: 'Media Gama',
      stars: 4.9,
    ),
    Character(
      name: 'Realme',
      age: 35,
      image: 'images/7.png',
      jobTitle: 'Baja Gama',
      stars: 3.8,
    ),
    Character(
      name: 'Redmi',
      age: 31,
      image: 'images/8.png',
      jobTitle: 'Media Gama',
      stars: 4.6,
    ),
    Character(
      name: 'Vivo',
      age: 22,
      image: 'images/9.png',
      jobTitle: 'Media Gama',
      stars: 4.0,
    ),
    Character(
      name: 'Huawei',
      age: 27,
      image: 'images/10.png',
      jobTitle: 'Baja Gama',
      stars: 3.9,
    ),
  ];

  void doSomething(Character character) {
    // ignore: avoid_print
    print(character.name);
  }
/*CODIGO AÑADIDO */

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cartonery List'),
      ),
      body: ListView(
        // TODO 1: Verifique la documentación oficial de ListView de Flutter.
        // Necesita pasar alguna propiedad a sus hijos.
        // Esta propiedad va a reemplazar el elemento null actual.
        // Si revisa el archivo characters_data.dart
        // encontrará que tiene acceso a una constante llamada "characters" de tipo "list of Character".
        // Usa aquí algún código funcional para convertir esa lista de datos en una lista de widgets que tienes
        // creado en TODO 0, revise el siguiente recurso: https://www.youtube.com/watch?v=R8rmfD9Y5-c
        // y tambien revisa el siguiente ejemplo: https://gitlab.com/aplicaciones-moviles-ii/flutter-ejercicio-5-listview-canciones.git
        // especialmente donde está la construcción del ListView.
        children: characters
            .map((character) =>
                CharacterWidget(character: character, onDoubleTap: doSomething))
            .toList(),
      ),
    );
  }
}
